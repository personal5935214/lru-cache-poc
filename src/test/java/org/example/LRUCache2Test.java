package org.example;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class LRUCache2Test {
    @Test
    public void testAddAndContainsKey() {
        LRUCache2 cache = new LRUCache2(3);

        cache.add("key1");
        assertTrue(cache.containsKey("key1"), "Added key should be present");

        cache.add("key2");
        assertTrue(cache.containsKey("key2"), "Added key should be present");

        cache.add("key3");
        assertTrue(cache.containsKey("key3"), "Added key should be present");

        assertFalse(cache.containsKey("key4"), "Key not added yet, should not be present");

        cache.add("key4");
        assertTrue(cache.containsKey("key4"), "Added key should be present");

        assertFalse(cache.containsKey("key1"), "Oldest key should have been removed");
    }

    @Test
    public void testSize() {
        LRUCache2 cache = new LRUCache2(3);

        assertEquals(0, cache.size(), "Cache should be empty initially");

        cache.add("key1");
        assertEquals(1, cache.size(), "Cache size should be 1 after adding one element");

        cache.add("key2");
        assertEquals(2, cache.size(), "Cache size should be 2 after adding two elements");

        cache.add("key3");
        assertEquals(3, cache.size(), "Cache size should be 3 after adding three elements");

        cache.add("key4");
        assertEquals(3, cache.size(), "Cache size should still be 3 after exceeding cache capacity");
    }

    @Test
    public void testConcurrentAccess() throws InterruptedException {
        // Initialize LRUCache with capacity 1000
        final int capacity = 1000;

        LRUCache2 cache = new LRUCache2(capacity);

        // Number of threads
        final int numThreads = 100;

        // Number of iterations per thread
        final int numIterations = 100000;

        // Executor service to manage threads
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        // Submit tasks to the executor
        for (int i = 0; i < numThreads; i++) {
            executorService.submit(() -> {
                // Access cache multiple times from each thread
                for (int j = 0; j < numIterations; j++) {
                    // Generate a unique key for each thread
                    String key = Thread.currentThread().getName() + "-" + j;
                    // Put value into cache
                    cache.add(key);
                    // Get value from cache
                    assertTrue(cache.containsKey(key));
                }
            });
        }

        // Shutdown executor and wait for all tasks to complete
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        // After all threads finish, assert that the cache size is within the capacity
        assertTrue(cache.size() <= capacity);
    }
}