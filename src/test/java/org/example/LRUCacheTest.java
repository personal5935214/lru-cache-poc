package org.example;


import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class LRUCacheTest {
    @Test
    public void testInsertWithinCapacity() {
        LRUCache<Integer> cache = new LRUCache<>(3);
        cache.put("A", 1);
        cache.put("B", 2);
        cache.put("C", 3);

        assertEquals(Integer.valueOf(1), cache.get("A"));
        assertEquals(Integer.valueOf(2), cache.get("B"));
        assertEquals(Integer.valueOf(3), cache.get("C"));
    }

    @Test
    public void testInsertBeyondCapacity() {
        LRUCache<Integer> cache = new LRUCache<>(2);
        cache.put("A", 1);
        cache.put("B", 2);
        cache.put("C", 3); // Beyond capacity

        assertNull(cache.get("A"));
        assertEquals(Integer.valueOf(2), cache.get("B"));
        assertEquals(Integer.valueOf(3), cache.get("C"));
    }

    @Test
    public void testRetrieveAndUpdate() {
        LRUCache<Integer> cache = new LRUCache<>(3);
        cache.put("A", 1);
        cache.put("B", 2);
        cache.put("C", 3);

        assertEquals(Integer.valueOf(1), cache.get("A"));
        cache.put("B", 5); // Update
        assertEquals(Integer.valueOf(5), cache.get("B"));
    }

    @Test
    public void testRetrieveNonExistent() {
        LRUCache<Integer> cache = new LRUCache<>(3);
        cache.put("A", 1);
        cache.put("B", 2);

        assertNull(cache.get("C"));
    }

    @Test
    public void testConcurrentAccess() throws InterruptedException {
        // Initialize LRUCache with capacity 1000
        final int capacity = 1000;

        LRUCache<Integer> cache = new LRUCache<>(capacity);

        // Number of threads
        final int numThreads = 100;

        // Number of iterations per thread
        final int numIterations = 100000;

        // Executor service to manage threads
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

        // Submit tasks to the executor
        for (int i = 0; i < numThreads; i++) {
            executorService.submit(() -> {
                // Access cache multiple times from each thread
                for (int j = 0; j < numIterations; j++) {
                    // Generate a unique key for each thread
                    String key = Thread.currentThread().getName() + "-" + j;
                    // Put value into cache
                    cache.put(key, j);
                    // Get value from cache
                    Integer value = cache.get(key);
                    // Ensure retrieved value is correct
                    assertNotNull(value);
                    assertEquals(j, value.intValue());
                }
            });
        }

        // Shutdown executor and wait for all tasks to complete
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        // After all threads finish, assert that the cache size is within the capacity
        assertTrue(cache.getSize() <= capacity);
    }

    @Test
    public void testNullKeyAndValue() {
        LRUCache<Integer> cache = new LRUCache<>(3);
        cache.put(null, 1);
        cache.put("A", null);

        assertNull(cache.get(null));
        assertNull(cache.get("A"));
    }

    @Test
    public void testCapacityZero() {
        LRUCache<Integer> cache = new LRUCache<>(0);
        cache.put("A", 1);
        assertNull(cache.get("A"));
    }

    @Test
    public void testLargeNumberOfElements() {
        LRUCache<Integer> cache = new LRUCache<>(1000);
        for (int i = 0; i < 10000; i++) {
            cache.put(String.valueOf(i), i);
        }
        assertEquals(Integer.valueOf(9990), cache.get("9990"));
    }

    @Test
    public void testMixedOperations() {
        LRUCache<Integer> cache = new LRUCache<>(3);
        cache.put("A", 1);
        cache.put("B", 2);
        cache.put("C", 3);
        cache.get("A"); // Access A
        cache.put("D", 4); // Evict B
        assertNull(cache.get("B"));
    }
}