package org.example;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LRUCache<T> {

    private final int capacity;
    private final AtomicInteger size;
    private final Map<String, Node> hashmap;
    private final DoublyLinkedList internalQueue;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    LRUCache(final int capacity) {
        this.capacity = capacity;
        this.hashmap = new HashMap<>();
        this.internalQueue = new DoublyLinkedList();
        this.size = new AtomicInteger();
    }

    public T get(final String key) {
        lock.writeLock().lock();

        try {
            Node node = hashmap.get(key);
            if (node == null) {
                return null;
            }
            internalQueue.moveNodeToFront(node);
            return node.value;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void put(final String key, final T value) {
        if (key == null || capacity == 0) {
            return;
        }

        lock.writeLock().lock();
        try {
            Node currentNode = hashmap.get(key);

            if (currentNode == null) {
                Node node = new Node(key, value);

                if (size.get() == capacity) {
                    Node prevTailNode = internalQueue.getPrevTailNode();
                    hashmap.remove(prevTailNode.key);
                    internalQueue.removeNode(prevTailNode);
                    size.decrementAndGet();
                }
                hashmap.put(key, node);
                internalQueue.addNodeToFront(node);
                size.incrementAndGet();
            } else {
                currentNode.value = value;
                internalQueue.moveNodeToFront(currentNode);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    public int getSize() {
        return size.get();
    }

    private class Node {
        String key;
        T value;
        Node next, prev;

        public Node(final String key, final T value) {
            this.key = key;
            this.value = value;
            this.next = null;
            this.prev = null;
        }
    }

    private class DoublyLinkedList {
        private final Node head, tail;

        public DoublyLinkedList() {
            head = new Node("", null);
            tail = new Node("", null);
            head.next = tail;
            tail.prev = head;
        }

        private void addNodeToFront(final Node node) {
            lock.writeLock().lock();
            try {
                Node nbr = head.next;

                node.next = head.next;
                node.prev = head;

                nbr.prev = node;
                head.next = node;
            } finally {
                lock.writeLock().unlock();
            }
        }

        public void moveNodeToFront(final Node node) {
            removeNode(node);
            addNodeToFront(node);
        }

        private void removeNode(Node node) {
            lock.writeLock().lock();
            try {
                Node prevNbr = node.prev;
                Node nextNbr = node.next;

                prevNbr.next = nextNbr;
                nextNbr.prev = prevNbr;

                node.next = node.prev = null;
            } finally {
                lock.writeLock().unlock();
            }
        }

        private Node getPrevTailNode() {
            lock.readLock().lock();
            try {
                return tail.prev;
            } finally {
                lock.readLock().unlock();
            }
        }
    }
}
