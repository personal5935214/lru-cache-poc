package org.example;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class LRUCache2 {
    private final Set<String> mySet;
    private final int cacheSize;

    public LRUCache2(int cacheSize) {
        this.cacheSize = cacheSize;
        mySet = Collections.newSetFromMap(new LinkedHashMap<String, Boolean>() {
            @Override
            protected boolean removeEldestEntry(Map.Entry<String, Boolean> eldest) {
                return size() > LRUCache2.this.cacheSize;
            }
        });
    }

    public synchronized void add(String key) {
        mySet.add(key);
    }

    public synchronized boolean containsKey(String key) {
        return mySet.contains(key);
    }

    public synchronized int size() {
        return mySet.size();
    }
}
